legacy-lifegame-ruby
=======================================

Ruby project for "Refactoring Legacy Code" workshop


SETUP
---------------------------------------

### with docker & docker-compose

```sh
$ docker-compose build
$ docker-compose up
```

entering CLI mode

```sh
$ docker-compose run --rm web bash
```

### local install

```sh
$ bundle install --path vendor/bundle
$ bundle exec ruby app.rb
```


DEMO
---------------------------------------

http://localhost:4567/lifegame
